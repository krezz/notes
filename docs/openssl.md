# OpenSSL Cheatsheet

## Base commands

### TLS Management

Create RSA private key:
```bash
openssl genrsa -out tls.key 2048
```

Create public key from private key:
```bash
openssl rsa -in tls.key -pubout -outform PEM -out tls.pub
```


### TLS Validation and Checks

> Note: MD5 sum must be the same

Get private key MD5 sum:
```bash
openssl rsa -noout -modulus -in tls.key | openssl md5
```

Get certificate MD5 sum:
```bash
openssl x509 -noout -modulus -in tls.crt | openssl md5
```

Get certificate request MD5 sum:
```bash
openssl req -noout -modulus -in tls.csr | openssl md5 
```

Get public key MD5 sum:
```bash
openssl rsa -noout -modulus -pubin -inform PEM -in tls.pub | openssl md5
```




